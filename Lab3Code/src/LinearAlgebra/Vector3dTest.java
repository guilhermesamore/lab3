// Guilherme Machado de Souza e Correa - 2035536
package LinearAlgebra;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class Vector3dTest {

    @Test
    void getters() {
        int x = 2;
        int y = 3;
        int z = 4;
        Vector3d vector = new Vector3d(x, y ,z);
        assertEquals(x, vector.getX());
        assertEquals(y, vector.getY());
        assertEquals(z, vector.getZ());
    }

    @Test
    void magnitude() {
        // According to wikipedia [https://en.wikipedia.org/wiki/Magnitude_(mathematics)],
        // the magnitude [3, 4, 12] is 13.
        Vector3d vector = new Vector3d(3, 4, 12);
        assertEquals(vector.magnitude(), 13);
    }

    @Test
    void dotProduct() {
        // According to wikipedia [https://en.wikipedia.org/wiki/Dot_product],
        // The dot product of [1, 3, -5] and [4, -2, -1] == 3
        Vector3d firstVector = new Vector3d(1, 3, -5);
        Vector3d secondVector = new Vector3d(4, -2, -1);
        assertEquals(3, firstVector.dotProduct(secondVector));
    }

    @Test
    void add() {
        Vector3d firstVector = new Vector3d(1, 1, 2);
        Vector3d secondVector = new Vector3d(2, 3, 4);

        Vector3d expectedResult = new Vector3d(3, 4, 6);
        Vector3d generatedResult = firstVector.add(secondVector);

        assertEquals(expectedResult.getX(), generatedResult.getX());
        assertEquals(expectedResult.getY(), generatedResult.getY());
        assertEquals(expectedResult.getZ(), generatedResult.getZ());
    }
}