// Guilherme Machado de Souza e Correa - 2035536
package LinearAlgebra;

public class Vector3d {
    final private double x;
    final private double y;
    final private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public double dotProduct(Vector3d vec) {
        double v = this.x * vec.x + this.y * vec.y +  this.z * vec.z;
        return v;
    }

    public Vector3d add(Vector3d vec) {
        return new Vector3d(this.x + vec.x, this.y + vec.y, this.z + vec.z);
    }
}
